import sqlite3


class Sqlite_connection:
    def __init__(self, base_name: str):
        try:
            self.sqlite_connection = sqlite3.connect(base_name)
            self.cursor = self.sqlite_connection.cursor()
            print("Соединение с базой данных ", base_name, ' успешно установлено')
        except sqlite3.Error as error:
            print("Ошибка при подключении к sqlite", error)

    def __del__(self):
        self.cursor.close()
        self.sqlite_connection.close()

    def get_value_by_key(self, table_name: str, key: str, property=None) -> str:
        query_text = f"SELECT Value from {table_name} WHERE Key = '{key}'"
        if property is not None:
            query_text = query_text + f" AND Property = '{property}'"
        self.cursor.execute(query_text)
        value = self.cursor.fetchone()
        if type(value) is not tuple:
            return value
        else:
            return value[0]


class Yougile_db(Sqlite_connection):
    def get_mail_property(self, key: str, property=None) -> str:
        return self.get_value_by_key('mail_requisits', key, property)

    def get_mail_properties(self, property_list: list) -> tuple:
        data_list = []
        for i in property_list:
            data_list.append(self.get_mail_property(*i))
        return tuple(data_list)

    def get_yougile_property(self, key: str, property=None) -> str:
        return self.get_value_by_key('yg_requisits', key, property)

    def get_yougile_properties(self, key: str, property_list: list) -> tuple:
        data_list = []
        for i in property_list:
            data_list.append(self.get_mail_property(*i))
        return tuple(data_list)

    def get_mail_uids(self) -> list:
        query_text = 'SELECT * from mails'
        self.cursor.execute(query_text)
        return [i[0] for i in self.cursor.fetchall()]

    def update_mail_uids(self, uid: str):
        query_text = f"INSERT INTO mails(uid) VALUES('{uid}')"
        self.cursor.execute(query_text)
        self.sqlite_connection.commit()

    def get_task_list(self) -> list:
        query_text = 'SELECT * from yg_tasks'
        self.cursor.execute(query_text)
        return self.cursor.fetchall()

    def update_task(self, task_id: str, number: str):
        query_text = f"INSERT INTO yg_tasks(id, Number) VALUES('{task_id}', '{number}')"
        self.cursor.execute(query_text)
        self.sqlite_connection.commit()

    def get_task_id_by_number(self, number: str) -> str:
        query_text = f"SELECT id FROM yg_tasks WHERE number = '{number}'"
        self.cursor.execute(query_text)
        id = self.cursor.fetchone()
        if id:
            return id[0]
        else:
            return None

    def get_column_id_from_status(self, status: str) ->str:
        if status == 'Create':
            return self.get_yougile_property('column_id', 'Принят в работу')
        elif status == '3rd line':
            return self.get_yougile_property('column_id', 'Передан на третью линию')
        elif status == 'Paused':
            return self.get_yougile_property('column_id', 'Приостановлен')
        elif status == 'Complete' or status == 'Note':
            return self.get_yougile_property('column_id', 'Выполнен')
