import imap_tools
from Jira_html_msg import Message_mail
from DB_API import Yougile_db
from Yougile_api import Yougile_API


def reversed_iterator(iter):
    return reversed(list(iter))

def mail_gen(uids: list, messages: list):
    for msg in messages:
        if msg.uid not in uids:
            yield msg


def main():
    yg_db = Yougile_db('Yougile_integration.db')

    mail, imap, password, korus_mail = yg_db.get_mail_properties(
        [('Mail', 'OKO'), ('IMAP', 'OKO'), ('Password', 'OKO'), ('Mail', 'Korus')])

    mb = imap_tools.MailBox(imap).login(mail, password)
    messages = mb.fetch(criteria=imap_tools.AND(from_=korus_mail),
                        limit=100,
                        mark_seen=False,
                        bulk=True,
                        reverse=True)

    files = []

    API = Yougile_API(yg_db)

    uids = yg_db.get_mail_uids()

    g = mail_gen(uids, reversed_iterator(messages))
    counter = 0
    for msg in g:
        message_obj = Message_mail(msg)
        if message_obj.type == 'Create':
            API.create_task_from_msg(message_obj)
        else:
            id = yg_db.get_task_id_by_number(message_obj.request_number)
            if id:
                API.update_task(message_obj, id)
        yg_db.update_mail_uids(message_obj.uid)
        print(message_obj.type)
        counter += 1
    del yg_db
    return counter

if __name__ == '__main__':
    main()

