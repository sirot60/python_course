def letters_range(start, stop=None, step=1):
    if not stop:
        stop = start
        start = 'a'
    list_ = []
    intstart = ord(start)
    intstop = ord(stop)
    for _ in range(intstart, intstop, step):
        list_.append(chr(_))
    return list_


print(letters_range('b', 'w', 2))
print(letters_range('g'))
print(letters_range('g', 'p'))
print(letters_range('p', 'g', -2))
print(letters_range('a'))
