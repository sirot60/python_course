import math


def atom(a=None):
    def get_value():
        return a

    def set_value(b):
        nonlocal a
        a = b
        return a

    def process_value(*args):
        nonlocal a
        for i in args:
            a = i(a)
        return a

    return get_value, set_value, process_value


get_value, set_value, process_value = atom(3)
print(get_value())
set_value(5)
process_value(math.sqrt)
print(get_value())
