def outside(f, counter_name):
    def wrapper(*args, **kwargs):
        globals()[counter_name] += 1
        f(*args, **kwargs)
        return f
    return wrapper

counter = 0
new = outside(min, 'counter')
new([5, 6])
new([6, 6])
new([7, 6])
print(counter)