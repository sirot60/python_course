from bs4 import BeautifulSoup
import imap_tools


class Message_mail:
    def __init__(self, msg: imap_tools.MailMessage):
        self.uid = msg.uid
        self.html = msg.html
        self.subject = msg.subject
        self.get_mail_type()
        self.get_mail_text()
        self.get_request_number()

    def get_mail_type(self):
        index = self.html.find("Вид письма - ")
        type_part = self.html[index + 13:]
        index = type_part.find(' -->')
        type_part = type_part[:index]
        if 'Мы получили' in type_part:
            self.type = 'Create'
        elif type_part == 'Работы по запросу приостановлены':
            if 'Запрос передан на 3-ю линию технической поддержки' in self.html:
                self.type = '3rd line'
            else:
                self.type = 'Paused'
        elif 'выполнен' in type_part:
            self.type = 'Complete'
        elif 'Оценки' or 'оцените' in self.html:
            self.type = 'Note'
        else:
            self.type = 'Mail'

    def get_mail_text(self):
        self.text = ''
        if self.type != 'Note':
            soup = BeautifulSoup(self.html)
            gen = self.msg_gen(soup.text)
            for i in gen:
                self.text += i

    def get_request_number(self):
        if 'оцените' in self.subject:
            self.subject.replace('[JIRA]', '')
            self.request_number = self.subject[self.subject.find('[') + 1:self.subject.find(']')]
        else:
            self.request_number = self.subject[self.subject.find('(') + 1:self.subject.find(')')]

    @staticmethod
    def msg_gen(text):
        boolean = False
        for i in text:
            if not boolean and i == '\n':
                boolean = True
                yield i
            elif i != '\n':
                if i != ' ':
                    boolean = False
                yield i
