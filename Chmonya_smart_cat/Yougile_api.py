import requests
from Jira_html_msg import Message_mail
from DB_API import Yougile_db
import json


class API_exception(Exception):
    def __init__(self, text, status_code):
        self.text = text
        self.status_code = status_code

class Yougile_API:
    def __init__(self, db: Yougile_db):
        self.db = db
        self.token = db.get_yougile_property('Token')

    def create_task_from_msg(self, msg: Message_mail):
        data = {'title': msg.subject, 'columnId': self.db.get_yougile_property('column_id', 'Принят в работу'),
                'description': msg.text}
        headers = {'Authorization': self.token, 'Content-Type': 'application/json'}
        j = json.dumps(data)
        r = requests.post(self.db.get_yougile_property('URL', 'Task'), headers=headers, data=j)
        if r.status_code == 201:
            self.db.update_task(json.loads(r.text)['id'], msg.request_number)
        else:
            raise API_exception('Ошибка запроса. Код', r.status_code)

    def update_task(self, msg: Message_mail, task_id):
        response = json.loads(self.get_task_by_id(task_id).text)
        response['description'] = response['description'] + '\n' + '-'*100 + '\n' + msg.text
        if msg.type != 'Mail':
            column = self.db.get_column_id_from_status(msg.type)
            response['columnId'] = column
        if msg.type == 'Note' or msg.type == 'Complete':
            response['completed'] = True
        else:
            response['completed'] = False
        del response['id']
        del response['timestamp']
        del response['createdBy']
        headers = {'Authorization': self.token, 'Content-Type': 'application/json'}
        request_body = json.dumps(response)
        response = requests.put(self.db.get_yougile_property('URL', 'Task') + f'/{task_id}', headers=headers, data=request_body)
        if response.status_code != 200:
            raise API_exception('Ошибка запроса. Код', response.status_code)

    def get_task_by_id(self, task_id):
        headers = {'Authorization': self.token}
        r = requests.get((self.db.get_yougile_property('URL', 'Task') + f'/{task_id}'), headers=headers)
        if r.status_code == 200:
            return r
        else:
            raise API_exception('Ошибка запроса. Код', r.status_code)


