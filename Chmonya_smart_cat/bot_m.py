from DB_API import Bot_db
import logging
from telegram.ext import CallbackContext, CommandHandler, MessageHandler, filters, ApplicationBuilder
import bot_commands

db = Bot_db('Chmonya.db')
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)




if __name__ == '__main__':
    application = ApplicationBuilder().token(db.get_value_by_key('properties', 'token', 'Telegram')).build()
    for i in db.get_bot_commands():
        application.add_handler(CommandHandler(i[0], getattr(bot_commands, i[1])))
    unknown_handler = MessageHandler(filters.COMMAND, bot_commands.unknown)
    application.add_handler(unknown_handler)
    echo_handler = MessageHandler(filters.TEXT & (~filters.COMMAND), bot_commands.echo)
    application.add_handler(echo_handler)
    application.run_polling()