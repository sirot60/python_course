from DB_API import Bot_db
from telegram import Update
from telegram.ext import CallbackContext
from youtubesearchpython import VideosSearch
import yg_int
import asyncio

def check_privilegies(username: str, function_name: str, db: Bot_db):
    function_priv = db.get_access_by_function_name(function_name)
    if function_priv == 'user':
        return True
    else:
        if db.search_for_user(username) != function_priv:
            return False
        else:
            return True


def command_decorator(f):
    async def wrapper(*args, **kwargs):
        db = Bot_db('Chmonya.db')
        db.save_message(args[0].message.from_user.username, args[0].message.text)
        if check_privilegies(args[0].message.from_user.username, f.__name__, db):
            kwargs = {'db': db}
            await f(*args, **kwargs)
        else:
            await args[1].bot.send_message(chat_id=args[0].effective_chat.id,
                                     text='У вас недостаточно прав для вызова этой команды. Обратитесь к '
                                          'администратору @sirota60')
        del db
    return wrapper


@command_decorator
async def echo(update: Update, context: CallbackContext, db):
    await context.bot.send_message(chat_id=update.effective_chat.id, text="Мяу")


@command_decorator
async def unknown(update: Update, context: CallbackContext, db):
    await context.bot.send_message(chat_id=update.effective_chat.id, text="Я пока этого не умею")

# async def start_quest(update: Update, context: CallbackContext, db)

@command_decorator
async def start(update: Update, context: CallbackContext, db):
    user = db.search_for_user(update.message.from_user.username)
    if not user:
        db.create_user(update.message.from_user.username)
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                 text=f'Привет, @{update.message.from_user.username}. '
                                      f' Вы пользуетесь ботом Чмоня - умный кот. Чтобы узнать, что я умею, напишите '
                                      f'моему хозяину @sirota60')
    else:
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                 text=f'Привет, @{update.message.from_user.username}. '
                                      f'Ты уже пользовался ботом. Твоя роль - {user}')
    del db


@command_decorator
async def video(update: Update, context: CallbackContext, db):
    video = VideosSearch(f'{update.message.text.replace("/video ", "")}', limit=1)
    await context.bot.send_message(chat_id=update.effective_chat.id, text=video.result().get('result')[0].get('link'))


@command_decorator
async def gain_access(update: Update, context: CallbackContext, db):
    if db.search_for_user(update.message.from_user.username)[0] != 'admin':
        if update.message.text.replace('/gainAccess ', '') != db.get_bot_property('admin', 'password'):
            await context.bot.send_message(chat_id=update.effective_chat.id, text='Неверный пароль')
        else:
            db.grant_admin(update.message.from_user.username)
            await context.bot.send_message(chat_id=update.effective_chat.id, text='Поздравляю. Вы теперь админ')
    else:
        await context.bot.send_message(chat_id=update.effective_chat.id, text='Вы и так админ. Зачем вам это?')


@command_decorator
async def update_yg(update: Update, context: CallbackContext, db):
    await context.bot.send_message(chat_id=update.effective_chat.id, text='Начинаю обновлять доску задач')
    await context.bot.send_message(chat_id=update.effective_chat.id,
                             text=f'Обновление завершено. Писем обработано: {await yg_int.main()}')

@command_decorator
async def grant_admin(update: Update, context: CallbackContext, db):
    splitted_string = update.message.text.split(' ')
    if len(splitted_string) != 2:
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                 text='Неверный формат команды')
    else:
        db.grant_admin(splitted_string[1].replace('@', ''))
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                 text='Права успешно выданы')