import sqlite3
import random
import time

class Sqlite_connection:
    @staticmethod
    def request_decorator(foo):
        def wrapper(*args, **kwargs):
            try:
                return foo(*args, **kwargs)
            except sqlite3.Error as error:
                randsec = random.random()
                print(f'{error}\n Retrying in {randsec} seconds')
                time.sleep(randsec)
                wrapper(*args, **kwargs)
        return wrapper


    def __init__(self, base_name: str):
        try:
            self.sqlite_connection = sqlite3.connect(base_name)
            self.cursor = self.sqlite_connection.cursor()
        except sqlite3.Error as error:
            pass
        random.seed()

    def __del__(self):
        self.cursor.close()
        self.sqlite_connection.close()

    @request_decorator
    def get_value_by_key(self, table_name: str, key: str, property=None) -> str:
        query_text = f"SELECT Value from {table_name} WHERE Key = '{key}'"
        if property is not None:
            query_text = query_text + f" AND Property = '{property}'"
        self.cursor.execute(query_text)
        value = self.cursor.fetchone()
        if type(value) is not tuple:
            return value
        else:
            return value[0]


class Bot_db(Sqlite_connection):
    @Sqlite_connection.request_decorator
    def get_bot_property(self, key: str, property=None):
        return self.get_value_by_key('properties', key, property)

    @Sqlite_connection.request_decorator
    def search_for_user(self, username):
        query_text = f"SELECT access_group from Users WHERE username = '{username}'"
        self.cursor.execute(query_text)
        result = self.cursor.fetchone()
        if type(result) is not tuple:
            return None
        else:
            return result[0]

    @Sqlite_connection.request_decorator
    def create_user(self, username):
        query_text = f"INSERT into Users(username, access_group) Values('{username}', 'user')"
        self.cursor.execute(query_text)
        self.sqlite_connection.commit()

    @Sqlite_connection.request_decorator
    def save_message(self, text, username):
        query_text = f"INSERT into messages(user, text) Values('{text}', '{username}')"
        self.cursor.execute(query_text)
        self.sqlite_connection.commit()

    @Sqlite_connection.request_decorator
    def grant_admin(self, username):
        query_text = f"UPDATE Users SET access_group = 'admin' WHERE username = '{username}'"
        self.cursor.execute(query_text)
        self.sqlite_connection.commit()

    @Sqlite_connection.request_decorator
    def get_bot_commands(self) -> list[tuple]:
        query_text = f"SELECT command_name, function_name FROM commands"
        self.cursor.execute(query_text)
        return self.cursor.fetchall()

    @Sqlite_connection.request_decorator
    def get_access_by_function_name(self, f_name):
        query_text = f"Select access_group from commands where function_name = '{f_name}'"
        self.cursor.execute(query_text)
        result = self.cursor.fetchone()
        if type(result) == tuple:
            return result[0]
        else:
            return None

class Yougile_db(Sqlite_connection):
    @Sqlite_connection.request_decorator
    def get_mail_property(self, key: str, property=None) -> str:
        return self.get_value_by_key('mail_requisits', key, property)

    @Sqlite_connection.request_decorator
    def get_mail_properties(self, property_list: list) -> tuple:
        data_list = []
        for i in property_list:
            data_list.append(self.get_mail_property(*i))
        return tuple(data_list)

    @Sqlite_connection.request_decorator
    def get_yougile_property(self, key: str, property=None) -> str:
        return self.get_value_by_key('yg_requisits', key, property)

    @Sqlite_connection.request_decorator
    def get_yougile_properties(self, key: str, property_list: list) -> tuple:
        data_list = []
        for i in property_list:
            data_list.append(self.get_mail_property(*i))
        return tuple(data_list)

    @Sqlite_connection.request_decorator
    def get_mail_uids(self) -> list:
        query_text = 'SELECT * from mails'
        self.cursor.execute(query_text)
        return [i[0] for i in self.cursor.fetchall()]

    @Sqlite_connection.request_decorator
    def update_mail_uids(self, uid: str):
        query_text = f"INSERT INTO mails(uid) VALUES('{uid}')"
        self.cursor.execute(query_text)
        self.sqlite_connection.commit()

    @Sqlite_connection.request_decorator
    def get_task_list(self) -> list:
        query_text = 'SELECT * from yg_tasks'
        self.cursor.execute(query_text)
        return self.cursor.fetchall()

    @Sqlite_connection.request_decorator
    def update_task(self, task_id: str, number: str):
        query_text = f"INSERT INTO yg_tasks(id, Number) VALUES('{task_id}', '{number}')"
        self.cursor.execute(query_text)
        self.sqlite_connection.commit()

    @Sqlite_connection.request_decorator
    def get_task_id_by_number(self, number: str) -> str | None:
        query_text = f"SELECT id FROM yg_tasks WHERE number = '{number}'"
        self.cursor.execute(query_text)
        id = self.cursor.fetchone()
        if id:
            return id[0]
        else:
            return None

    @Sqlite_connection.request_decorator
    def get_column_id_from_status(self, status: str) -> str | None:
        if status == 'Create':
            return self.get_yougile_property('column_id', 'Принят в работу')
        elif status == '3rd line':
            return self.get_yougile_property('column_id', 'Передан на третью линию')
        elif status == 'Paused':
            return self.get_yougile_property('column_id', 'Приостановлен')
        elif status == 'Complete' or status == 'Note':
            return self.get_yougile_property('column_id', 'Выполнен')
